package Test_Package;

import java.time.LocalDateTime;

import org.testng.Assert;

import AllMethods.Api_Trigger;
//import AllMethods.Utility1;
import Respository1.RequestBody1;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ResponseBody;

public class Test_Case1 extends RequestBody1 {

	public static void main(String[] args) {

		// Utility1.CreateLogDirectory("Post_API_Logs1");

		String Endpoint = RequestBody1.Hostname() + RequestBody1.PostResource();

		ResponseBody res_body = Api_Trigger.Post_Trigger(RequestBody1.HeaderName(), RequestBody1.HeaderValue(),
				RequestBody1.req_tc1(), Endpoint);

		String res_name = res_body.jsonPath().getString("name");
		System.out.println(res_name);
		String res_job = res_body.jsonPath().getString("job");
		System.out.println(res_job);
		String res_id = res_body.jsonPath().getString("id");
		System.out.println(res_id);
		String res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);
		System.out.println(res_createdAt);

		// Step 5 : Validate the response body

		// Step 5.1 : Parse request body and save into local variables

		JsonPath jsp_req = new JsonPath(req_tc1());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Step 5.2 : Generate expected date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Step 5.3 : Use TestNG's Assert

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);
	}

}
