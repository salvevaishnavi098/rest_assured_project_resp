package Test_Package;

import java.time.LocalDateTime;

import org.testng.Assert;

import AllMethods.Api_Trigger;
import Respository1.RequestBody1;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_Put extends RequestBody1 {

	public static void main(String[] args) {
		String Endpoint = RequestBody1.Hostname() + RequestBody1.PutResource();

		ResponseBody res_body = Api_Trigger.Put_Trigger(RequestBody1.HeaderName(), RequestBody1.HeaderValue(),
				RequestBody1.req_Put(), Endpoint);

		 //int statuscode = response.statusCode();
		 //System.out.println(response.statusCode());
		// ResponseBody<?> res_body = response.getBody();
		System.out.println(res_body.asString());
		String res_name = res_body.jsonPath().getString("name");
		System.out.println(res_name);
		String res_job = res_body.jsonPath().getString("job");
		System.out.println(res_job);
		String res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
		System.out.println(res_updatedAt);

		// Step 5 : Validate the response body

		// Step 5.1 : Parse request body and save into local variables

		JsonPath jsp_req = new JsonPath(req_Put());
		String req_name = jsp_req.getString("name");
		// System.out.println(req_name);
		String req_job = jsp_req.getString("job");
		// System.out.println(req_job);

		// Step 5.2 : Generate expected date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Step 5.3 : Use TestNG's Assert

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
	}

}
