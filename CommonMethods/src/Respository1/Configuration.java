package Respository1;

public class Configuration {

	public static String Hostname(){
      String hostname = "https://reqres.in/";
		return hostname;
	}

	public static String PostResource() {
		String resource = "api/users";
		return resource;
	}
	
	public static String PutResource() {
		String resource = "/api/users/2";
		return resource;
	}
	
	public static String PatchResource() {
		String resource = "/api/users/2";
		return resource;
	}

	public static String HeaderName() {
		String headername = "Content-Type";
		return headername;
	}

	public static String HeaderValue() {
		String headervalue = "application/json";
		return headervalue;
	}

}
