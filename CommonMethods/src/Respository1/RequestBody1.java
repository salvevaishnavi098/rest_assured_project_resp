package Respository1;

public class RequestBody1 extends Configuration {
	
	public static String req_tc1() {
		
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		return req_body;
	}
	
public static String req_tc2() {
		
		String req_body = "{\r\n" + "    \"name\": \"vaishnavi\",\r\n" + "    \"job\": \"OA\"\r\n" + "}";
		return req_body;

}

public static String req_Put() {
	
	String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
	return req_body;
}

public static String req_Patch() {

	String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
	return req_body;
}
}
