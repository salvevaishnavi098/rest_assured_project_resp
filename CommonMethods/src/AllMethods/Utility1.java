package AllMethods;

import java.io.File;

public class Utility1 {

	
	public static File CreateLogDirectory(String dirName) {

		// Step 1 : Fetch the java project and location

		String projectDir = System.getProperty("user.dir");
		System.out.println("Current Project Directory is :" + projectDir);

		// Step 2 : Verify whether the directory in variable dirName exists inside
		// projectDir and act accordingly

		File directory = new File(projectDir + "\\" + dirName);

		if (directory.exists()) {
			System.out.println(directory + ", Already Exists");
		} else {

			System.out.println(directory + ", Dosen't Exists , hence Creating it");
			directory.mkdir();
			System.out.println((directory + ", created"));

		}

		return directory;

	}

}
