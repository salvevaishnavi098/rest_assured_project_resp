package Runner;

import java.io.IOException;

import TestCases.Test_Case_1;

import TestCases.Test_Case_2;

//import Comman_Methods.Utility;


public class TestCase_Runner {

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		Test_Case_1.executor();
		Test_Case_2.executor();

		// Utility.readExcelData("Post_API", "Post_TC1");

	}

}
