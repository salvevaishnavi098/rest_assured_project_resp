package Request_specification_Response_Class;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
//import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_Post_Register_Successful {

	public static void main(String[] args) {

		// Step 1 : Collect all needed information and save it into local variables
		String req_body = "{\r\n" + "    \"email\": \"eve.holt@reqres.in\",\r\n" + "    \"password\": \"pistol\"\r\n"
				+ "}";
		String hostname = "https://reqres.in/";
		String resource = "api/register";
		String headername = "Content-Type";

		String headervalue = "application/json";

		// step 2 Build the request specification using request specification class
		RequestSpecification requestspec = RestAssured.given();

		// Step 2.1 : Set request header
		requestspec.header(headername, headervalue);

		// Step 2.2 : Set request body
		requestspec.body(req_body);

		// step 3 send the api request
		Response response = requestspec.post(hostname + resource);
		System.out.println(response.getBody().asString());

		// Step 4 : Parse the response body
		ResponseBody res_body = response.getBody();

		String res_id = res_body.jsonPath().getString("id");
		System.out.println(res_id);
		String res_token = res_body.jsonPath().getString("token");
		System.out.println(res_token);

		// Step 4.1 : Parse the request body

		JsonPath jsp_req = new JsonPath(req_body);
		String req_email = jsp_req.getString("email");
		System.out.println(req_email);
		String req_password = jsp_req.getString("password");
		System.out.println(req_password);

		// Step 5 : Use TestNG's Assert for validation

		Assert.assertNotNull(res_token);
		Assert.assertNotNull(res_id);

	}

}
