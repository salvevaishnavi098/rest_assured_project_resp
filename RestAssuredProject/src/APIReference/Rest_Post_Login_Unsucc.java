package APIReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Rest_Post_Login_Unsucc {

	public static void main(String[] args) {

		// Step 1 : Collect all need information and save in local variable

		String req_body = "{\r\n" + "    \"email\": \"peter@klaven\"\r\n" + "}";

		String hostname = "https://reqres.in/";

		String resource = "/api/login";

		String headername = "Content-Type";

		String headervalue = "application/json";

		// Step 2 : Declare base URI

		RestAssured.baseURI = hostname;

		// Step 3 : Configure the execution of API and save as a String

		String res_body = given().header(headername, headervalue).body(req_body).when().post(resource).then().extract()
				.response().asString();

		System.out.println(res_body);

		// Step 4 : parse the response body

		// Step 4.1 : create object of JsonPath for response body

		JsonPath jsp_res = new JsonPath(res_body);

		// Step 4.2 : parse the individual parameter by using jsp_res

		String res_error = jsp_res.getString("error");
		System.out.println(res_error);

		// Step 5 : Validate the response body

		// Step 5.1 : Parse the request body and save the variable in local and create
		// JsonPath for Request body

		JsonPath jsp_req = new JsonPath(req_body);
		String req_email = jsp_req.getString("email");
		System.out.println(req_email);

		// Step 6 : Use TestNG Assert

		Assert.assertNotNull(res_error);

	}

}
