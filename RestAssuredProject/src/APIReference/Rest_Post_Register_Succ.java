package APIReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Rest_Post_Register_Succ {

	public static void main(String[] args) {

		// Step 1 : Collect all need information and save in local variable

		String req_body = "{\r\n" + "    \"email\": \"eve.holt@reqres.in\",\r\n" + "    \"password\": \"pistol\"\r\n"
				+ "}";

		String hostname = "https://reqres.in/";

		String resource = "/api/register";

		String headername = "Content-Type";

		String headervalue = "application/json";

		// Step 2 : Declare Base value

		RestAssured.baseURI = hostname;

		// STep 3 : Configure the API execution and save response in String Variables

		String res_body = given().header(headername, headervalue).body(req_body).when().post(resource).then().extract()
				.response().asString();

		System.out.println(res_body);

		// Step 4 : Parse the response body

		// Step 4.1 : create the object of JsonPath for response body

		JsonPath jsp_res = new JsonPath(res_body);

		// Step 4.2 : Parse the all individual parameter by using jsp_res object

		String res_id = jsp_res.getString("id");
		System.out.println(res_id);

		String res_token = jsp_res.getString("token");
		System.out.println(res_token);

		// Step 5 : validate response body

		// Step 5.1 : create the object of JsonPath for request body and parse the
		// request body and save into local variable

		JsonPath jsp_req = new JsonPath(req_body);
		String req_email = jsp_req.getString("email");
		System.out.println(req_email);
		String req_password = jsp_req.getString("password");
		System.out.println(req_password);

		// Step 6 : TestNG Assert

		Assert.assertNotNull(res_id);
		Assert.assertNotNull(res_token);

	}

}
