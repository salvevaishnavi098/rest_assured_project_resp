package APIReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Rest_Post_Register_Unsucc {

	public static void main(String[] args) {

		// Step 1 : collect all information and save into local variables

		String req_body = "{\r\n" + "    \"email\": \"sydney@fife\"\r\n" + "}";

		String hostname = "https://reqres.in/";

		String resource = "/api/register";

		String headername = "Content-Type";

		String headervalue = "application/json";

		// Step 2 : Declare base URL

		RestAssured.baseURI = hostname;

		// Step 3 : Configure the API for execution and save the response in local
		// variable

		String res_body = given().header(headername, headervalue).body(req_body).when().post(resource).then().extract()
				.response().asString();

		System.out.println(res_body);

		// Step 4 : Parse the response body

		// Step 4.1 : create the object of JsonPath for response body

		JsonPath jsp_res = new JsonPath(res_body);

		// Step 4.2 : Parse the individual parameter by using jsp_res Object

		String res_error = jsp_res.getString("error");
		System.out.println(res_error);

		// Step 5 : Parse the request body and create object of JsonPath for request
		// body

		JsonPath jsp_req = new JsonPath(req_body);

		String req_email = jsp_req.getString("email");
		System.out.println(req_email);

		// Step 6 : Use TestNG assert

		Assert.assertNotNull(res_error);

	}

}
