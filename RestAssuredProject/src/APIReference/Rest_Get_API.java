package APIReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Rest_Get_API {

	public static void main(String[] args) {
		
	// Step 1 :  Collect the all needed information and save it in local variables
		
		//String req_body ="";
		
		String hostname = "https://reqres.in/";
		
		String resource = "/api/users?page=2";
		
		String headername = "Content-Type";
		
		String headervalue = "Application/json";
		
		// Step 2 : Declare Base Value
		
		RestAssured.baseURI=hostname;
		
		// Step 3 : Configure the API for execution and save the response in String Variable
		
		String res_body = given().header(headervalue, headername).when().get(resource).then().extract().response().asString();
		System.out.println(res_body);
		
		
	   // Step 4 : Parse the response body
		
		// Step 4.1 : Create the object of JsonPath
		
		JsonPath jsp_res = new JsonPath(res_body);
		
		// Step 4.2 :- Parse individual value parameters using jsp_res Object
		
		String res_id = jsp_res.getString("id");
		System.out.println(res_id);
		String res_email =jsp_res.getString("email");
		System.out.println(res_email);
		String res_first_name = jsp_res.getString("first_name");
		System.out.println(res_first_name);
		String res_last_name = jsp_res.getString("last_name");
		System.out.println(res_last_name);
		String res_avatar = jsp_res.getString("avatar");
		System.out.println(res_avatar);
		
		// Step 5 : validate the response body
		
		// Step 5.1 : Parse request body and save into local variables
		
		/*JsonPath jsp_req = new JsonPath(req_body);
		
		String req_id = jsp_req.getString("id");
		String req_email = jsp_req.getString("email");
		String req_first_name = jsp_req.getString("first_name");
		String req_last_name = jsp_req.getString("last_name");
		String req_avatar = jsp_req.getString("avatar");*/
		
		//Step 5.1 : store the expected result
		 String exp_id = "[8,9,10,11'12]";
		 String exp_email = "[michael.lawson@reqres.in,lindsay.ferguson@reqres.in,tobias.funke@reqres.in,byron.fields@reqres.in,george.edwards@reqres.in,rachel.howell@reqres.in]";
		 String exp_first_name = "[Michael,Lindsay,Tobias,Byron,George,Rachel]";
		 String exp_last_name = "[Lawson,Ferguson,Funke,Fields,Edwards,Howell]";
		 String exp_avatar = "[https://reqres.in/img/faces/7-image.jpg,https://reqres.in/img/faces/8-image.jpg,https://reqres.in/img/faces/9-image.jpg,https://reqres.in/img/faces/10-image.jpg,https://reqres.in/img/faces/11-image.jpg,https://reqres.in/img/faces/12-image.jpg]";
		 
		 
		// Step 5.2 : Use TestNG's Assert
		
		Assert.assertEquals(res_id, exp_id);
		Assert.assertEquals(res_email, exp_email);
		Assert.assertEquals(res_first_name, exp_first_name);
		Assert.assertEquals(res_last_name, exp_last_name);
		Assert.assertEquals(res_avatar, exp_avatar);
		
		
		

	}

}
