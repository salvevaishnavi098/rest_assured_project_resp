package APIReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Rest_Post_Login_Succ {

	public static void main(String[] args) {

		// Step 1 : COllect all needed information and save into local variable

		String req_body = "{\r\n" + "    \"email\": \"eve.holt@reqres.in\",\r\n"
				+ "    \"password\": \"cityslicka\"\r\n" + "}";

		String hostname = "https://reqres.in/";

		String resource = "/api/login";

		String headername = "Content-Type";

		String headervalue = "application/json";

		// Step 2 : Declare base URI

		RestAssured.baseURI = hostname;

		// Step 3 : Configure the API execution and save response in string

		String res_body = given().header(headername, headervalue).body(req_body).when().post(resource).then().extract()
				.response().asString();

		System.out.println(res_body);

		// Step 4 : parse the response body

		// Step 4.1 : create object of JsonPath for response body

		JsonPath jsp_res = new JsonPath(res_body);

		// Step 4.2 : Parse the individual parameter with jsp_res Object

		String res_token = jsp_res.getString("token");
		System.out.println(res_token);

		// Step 5 : Validate the response body

		// Step 5.1 : Parse the request body and save into local variable as well as
		// create JsonPath for request body

		JsonPath jsp_req = new JsonPath(req_body);
		String req_email = jsp_req.getString("email");
		System.out.println(req_email);

		String req_password = jsp_req.getString("password");
		System.out.println(req_password);

		// Step 6 : Use TestNG Assert

		Assert.assertNotNull(res_token);

	}

}
