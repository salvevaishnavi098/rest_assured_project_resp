Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Tue, 27 Feb 2024 19:30:19 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"806","createdAt":"2024-02-27T19:30:19.321Z"}